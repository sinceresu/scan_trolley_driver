#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/image_encodings.h"
#include "sensor_msgs/Image.h"
#include <opencv2/opencv.hpp>

#include <spinnaker_publisher/spinnaker_publisher.hpp>

using namespace std;

#define MAX_BUFF_LEN 256
std::string g_error_info_str;

class SpinnakerCamera
{
    friend class SpinnakerManager;
public:
    bool beginAcquisition()
    {
        spinError err = spinCameraBeginAcquisition(_camera);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("[SpinnakerCamera] Unable to begin image acquisition: %d\n", err);
            return false;
        }

        _acquisitionEnabled = true;
        return true;
    }

    bool endAcquisition()
    {
        spinError err = spinCameraEndAcquisition(_camera);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("[SpinnakerCamera] Unable to end image acquisition: %d\n", err);
            return false;
        }

        _acquisitionEnabled = false;
        return true;
    }

    cv::Mat getFrame(ros::Time& frame_time)
    {
        cv::Mat frame;
        if (!_acquisitionEnabled) beginAcquisition();
        if (!_acquisitionEnabled) return frame;

        spinImage resultImage = NULL;
        uint64_t grab_timeout= 1000;
        spinError err = spinCameraGetNextImageEx(_camera, grab_timeout, &resultImage);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("[SpinnakerCamera] Unable to get next image: %d\n", err);
            return frame;
        }
        frame_counter++;
        if (skip_frames>0) {
            if ((frame_counter % skip_frames) != 0) {
                spinImageRelease(resultImage);
                return frame;
            } 
        }
        frame_time=  ros::Time::now();
        // Print image information
        /*size_t width = 0;
        size_t height = 0;
        err = spinImageGetWidth(resultImage, &width);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to retrieve image width. Non-fatal error %d...\n", err);
        }
        err = spinImageGetHeight(resultImage, &height);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to retrieve image height. Non-fatal error %d...\n", err);
        }*/

        bool8_t isIncomplete = False;
        err = spinImageIsIncomplete(resultImage, &isIncomplete);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("[SpinnakerCamera] Unable to determine image completion: %d\n", err);
            spinImageRelease(resultImage); return frame;
        }
        else if (isIncomplete)
        {
            spinImageStatus imageStatus = IMAGE_NO_ERROR;
            spinImageGetStatus(resultImage, &imageStatus);
            printf("[SpinnakerCamera] Image incomplete with image status %d\n", imageStatus);
            spinImageRelease(resultImage); return frame;
        }

        spinImage convertedImage = NULL;
        spinImageCreateEmpty(&convertedImage);
        spinImageConvert(resultImage, PixelFormat_RGB8, convertedImage);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to convert image. Non-fatal error %d...\n\n", err);
            return frame;
        }

        /*
        // Create unique file name
        char filename[MAX_BUFF_LEN];
        sprintf(filename, "ImageFormatControl-C.jpg");
        // Save image
        err = spinImageSave(convertedImage, filename, JPEG);
        if (err != SPINNAKER_ERR_SUCCESS){
            printf("Unable to save image. Non-fatal error %d...\n", err);
        }
        else{
            printf("Image saved at %s\n\n", filename);
        }
        */

        auto timestamp = getInternalImage(convertedImage, frame);
        spinImageDestroy(convertedImage);
        spinImageRelease(resultImage);
        return frame;
    }

    uint64_t getInternalImage(spinImage resultImage, cv::Mat& frame)
    {
        size_t width = 0, height = 0, bufferSize = 0, stride = 0;
        spinPixelFormatEnums pixelFormat = PixelFormat_RGB8;
        spinImageGetWidth(resultImage, &width);
        spinImageGetHeight(resultImage, &height);
        spinImageGetStride(resultImage, &stride);
        spinImageGetPixelFormat(resultImage, &pixelFormat);
        spinImageGetBufferSize(resultImage, &bufferSize);
        //std::cout << "width: " << width << " ; height: " << height << "; stride: " << stride << "; pixelFormat: " << pixelFormat << "; bufferSize: " << bufferSize << std::endl;

        uint64_t timestamp = 0;
        void* data = NULL;
        spinImageGetData(resultImage, &data);
        spinImageGetTimeStamp(resultImage, &timestamp);

        switch (pixelFormat)
        {
        case PixelFormat_Mono8:
            frame = cv::Mat(height, width, CV_8UC1);
            memcpy(frame.data, data, bufferSize);
            break;
        case PixelFormat_RGB8:
            frame = cv::Mat(height, width, CV_8UC3);
            memcpy(frame.data, data, bufferSize);
            break;
        default:
            printf("[SpinnakerCamera] Unsupported image pixel format %d\n", pixelFormat);
            break;
        }
        return timestamp;
    }

    spinError ConfigureCustomImageSettings(spinNodeMapHandle hNodeMap)
    {
        spinError err = SPINNAKER_ERR_SUCCESS;
        printf("\n\n*** CONFIGURING CUSTOM IMAGE SETTINGS ***\n\n");
        spinNodeHandle hPixelFormat = NULL;
        spinNodeHandle hPixelFormatRGB8 = NULL;
        int64_t pixelFormatRGB8 = 0;

        // Retrieve enumeration node from the nodemap
        err = spinNodeMapGetNode(hNodeMap, "PixelFormat", &hPixelFormat);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to set pixel format (node retrieval). Aborting with error %d...\n\n", err);
            return err;
        }

        // Retrieve desired entry node from the enumeration node
        if (IsAvailableAndReadable(hPixelFormat, "PixelFormat"))
        {
            err = spinEnumerationGetEntryByName(hPixelFormat, "RGB8", &hPixelFormatRGB8);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to set pixel format (enum entry retrieval). Aborting with error %d...\n\n", err);
                return err;
            }
        }
        else
        {
            PrintRetrieveNodeFailure("node", "PixelFormat");
            return SPINNAKER_ERR_ACCESS_DENIED;
        }

        // Retrieve integer value from entry node
        if (IsAvailableAndReadable(hPixelFormatRGB8, "PixelFormatRGB8"))
        {
            err = spinEnumerationEntryGetIntValue(hPixelFormatRGB8, &pixelFormatRGB8);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to set pixel format (enum entry int value retrieval). Aborting with error %d...\n\n", err);
                return err;
            }
        }
        else
        {
            PrintRetrieveNodeFailure("entry", "PixelFormat 'RGB8'");
            return SPINNAKER_ERR_ACCESS_DENIED;
        }

        // Set integer as new value for enumeration node
        if (IsAvailableAndWritable(hPixelFormat, "PixelFormat"))
        {
            err = spinEnumerationSetIntValue(hPixelFormat, pixelFormatRGB8);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to set pixel format (enum entry setting). Aborting with error %d...\n\n", err);
                return err;
            }
        }
        else
        {
            PrintRetrieveNodeFailure("node", "PixelFormat");
            return SPINNAKER_ERR_ACCESS_DENIED;
        }

        printf("Pixel format set to 'RGB8'...\n\n\n\n");
        return SPINNAKER_ERR_SUCCESS;
    }

  spinError setFrameRate(float frame_rate)
    {
        spinError err = SPINNAKER_ERR_SUCCESS;
        spinNodeHandle hFrameRate = NULL;

        err = spinNodeMapGetNode(_nodeMap, "AcquisitionFrameRate", &hFrameRate);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to set get frame rate format (node retrieval). Aborting with error %d...\n\n", err);
            return err;
        }
        uint8_t ok;
        spinNodeIsReadable(hFrameRate, &ok);
        if ( ok)
        {   
            double acquisition_frame_rate;
            err = spinFloatGetValue(hFrameRate, &acquisition_frame_rate);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to get frame rate. Aborting with error %d...\n", err);
                return err ;
            }
            if (frame_rate < acquisition_frame_rate) {
                frame_rate = max(0.2f, frame_rate);
               skip_frames = int(acquisition_frame_rate / frame_rate);
            }
         }
        return SPINNAKER_ERR_SUCCESS;
     }
     spinError ConfigureCustomExposureSettings(std::string exposure_mode, int exposure_value)
     {
         spinError err = SPINNAKER_ERR_SUCCESS;
        if(exposure_mode == "Continuous") {
            setAttributeNodeValue("ExposureAuto", "ExposureAutoOff", "Continuous");
        }
        else if(exposure_mode == "Once"){
            setAttributeNodeValue("ExposureAuto", "ExposureAutoOff", "Once");
        }
        else if(exposure_mode == "Off"){
            setAttributeNodeValue("ExposureAuto", "ExposureAutoOff", "Off");
        }

        // Set exposure time manually; exposure time recorded in microseconds
        //
        // *** NOTES ***
        // It is ensured that the desired exposure time does not exceed the maximum.
        // Exposure time is counted in microseconds - this can be found out either
        // by retrieving the unit with the spinFloatGetUnit() methods or by
        // checking SpinView.
        //
        if(exposure_mode == "Off"){
            spinNodeHandle hExposureTime = NULL;
            double exposureTimeMax = 0.0;
            double exposureTimeToSet = exposure_value;

            // Retrieve exposure time node
            err = spinNodeMapGetNode(_nodeMap, "ExposureTime", &hExposureTime);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to set exposure time. Aborting with error %d...\n", err);
                return err;
            }

            // Retrieve maximum value
            if (IsAvailableAndReadable(hExposureTime, "ExposureTime"))
            {
                err = spinFloatGetMax(hExposureTime, &exposureTimeMax);
                if (err != SPINNAKER_ERR_SUCCESS)
                {
                    printf("Unable to set exposure time. Aborting with error %d...\n", err);
                    return err;
                }
            }
            else
            {
                PrintRetrieveNodeFailure("node", "ExposureTime");
                return SPINNAKER_ERR_ACCESS_DENIED;
            }

            // Ensure desired exposure time does not exceed maximum
            if (exposureTimeToSet > exposureTimeMax)
            {
                exposureTimeToSet = exposureTimeMax;
            }

            // Set desired exposure time as new value
            if (IsAvailableAndWritable(hExposureTime, "ExposureTime"))
            {
                err = spinFloatSetValue(hExposureTime, exposureTimeToSet);
                if (err != SPINNAKER_ERR_SUCCESS)
                {
                    printf("Unable to set exposure time. Aborting with error %d...\n", err);
                    return err;
                }

                //printf("Exposure time set to %f us...\n", exposureTimeToSet);
            }
            else
            {
                PrintRetrieveNodeFailure("node", "ExposureTime");
                return SPINNAKER_ERR_ACCESS_DENIED;
            }
        }
     }

    spinError PrintDeviceInfo(spinNodeMapHandle hNodeMap)
    {
        spinError err = SPINNAKER_ERR_SUCCESS;
        unsigned int i = 0;
        printf("\n*** DEVICE INFORMATION ***\n\n");

        // Retrieve device information category node
        spinNodeHandle hDeviceInformation = NULL;
        err = spinNodeMapGetNode(hNodeMap, "DeviceInformation", &hDeviceInformation);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to retrieve node. Non-fatal error %d...\n\n", err);
            return err;
        }

        // Retrieve number of nodes within device information node
        size_t numFeatures = 0;
        if (IsAvailableAndReadable(hDeviceInformation, "DeviceInformation"))
        {
            err = spinCategoryGetNumFeatures(hDeviceInformation, &numFeatures);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to retrieve number of nodes. Non-fatal error %d...\n\n", err);
                return err;
            }
        }
        else
        {
            PrintRetrieveNodeFailure("node", "DeviceInformation");
            return SPINNAKER_ERR_ACCESS_DENIED;
        }

        // Iterate through nodes and print information
        for (i = 0; i < numFeatures; i++)
        {
            spinNodeHandle hFeatureNode = NULL;
            err = spinCategoryGetFeatureByIndex(hDeviceInformation, i, &hFeatureNode);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to retrieve node. Non-fatal error %d...\n\n", err);
                continue;
            }

            spinNodeType featureType = UnknownNode;
            // Get feature node name
            char featureName[MAX_BUFF_LEN];
            size_t lenFeatureName = MAX_BUFF_LEN;
            err = spinNodeGetName(hFeatureNode, featureName, &lenFeatureName);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                strcpy(featureName, "Unknown name");
            }

            if (IsAvailableAndReadable(hFeatureNode, featureName))
            {
                err = spinNodeGetType(hFeatureNode, &featureType);
                if (err != SPINNAKER_ERR_SUCCESS)
                {
                    printf("Unable to retrieve node type. Non-fatal error %d...\n\n", err);
                    continue;
                }
            }
            else
            {
                printf("%s: Node not readable\n", featureName);
                continue;
            }

            char featureValue[MAX_BUFF_LEN];
            size_t lenFeatureValue = MAX_BUFF_LEN;
            err = spinNodeToString(hFeatureNode, featureValue, &lenFeatureValue);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                strcpy(featureValue, "Unknown value");
            }

            printf("%s: %s\n", featureName, featureValue);
        }
        printf("\n");

        return err;
    }

protected:
    SpinnakerCamera(spinCamera cam) : _camera(cam), _acquisitionEnabled(false),frame_counter(0)
    {
        spinError err = SPINNAKER_ERR_SUCCESS;
        // Retrieve TL device nodemap and print device information
        err = spinCameraGetTLDeviceNodeMap(_camera, &_hNodeMapTLDevice);
        if (err != SPINNAKER_ERR_SUCCESS){
            printf("Unable to retrieve TL device nodemap. Non-fatal error %d...\n\n", err);
        }
        else{
            err = PrintDeviceInfo(_hNodeMapTLDevice);
        }

        // Initialize camera
        err = spinCameraInit(_camera);
        if (err != SPINNAKER_ERR_SUCCESS){
            printf("Unable to initialize camera. Aborting with error %d...\n\n", err);
            return;
        }

        // Retrieve GenICam nodemap
        err = spinCameraGetNodeMap(_camera, &_nodeMap);
        if (err != SPINNAKER_ERR_SUCCESS){
            printf("Unable to retrieve GenICam nodemap. Aborting with error %d...\n\n", err);
            return;
        }
        // Configure custom image settings
        err = ConfigureCustomImageSettings(_nodeMap);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Configure error!");
            return;
        }

        // Set continuous acquisition mode and exposure auto-off
        setAttributeNodeValue("AcquisitionMode", "AcquisitionModeContinuous", "Continuous");
        setAttributeNodeValue("ExposureAuto", "ExposureAutoOff", "Continuous");

        // Set stream buffer handling mode
        err = spinCameraGetTLStreamNodeMap(_camera, &_streamNodeMap);
        if (err != SPINNAKER_ERR_SUCCESS)
            printf("[SpinnakerCamera] Unable to retrieve TLStream nodemap: %d\n", err);

        err = spinNodeMapGetNode(_streamNodeMap, "StreamBufferHandlingMode", &_streamBufferMode);
        if (err != SPINNAKER_ERR_SUCCESS)
            printf("[SpinnakerCamera] Unable to set stream-buffer mode: %d\n", err);

        int64_t streamBufferModeNewestFirstValue = StreamBufferHandlingMode_NewestOnly;
        if (IsAvailableAndWritable(_streamBufferMode, "StreamBufferHandlingMode"))
            spinEnumerationSetIntValue(_streamBufferMode, streamBufferModeNewestFirstValue);

        // Acquire images
        //err = AcquireImages(_camera, _nodeMap, _hNodeMapTLDevice);
        //if (err != SPINNAKER_ERR_SUCCESS){return;}
    }

    ~SpinnakerCamera()
    {
        if (_camera != NULL)
        {
            spinCameraDeInit(_camera);
            spinCameraRelease(_camera);
        }
    }

    void setAttributeNodeValue(char* nodeName,  char* valueName, const char* value)
    {
        spinNodeHandle attrNode = NULL, valueNode = NULL;
        spinError err = spinNodeMapGetNode(_nodeMap, nodeName, &attrNode);
        if (err != SPINNAKER_ERR_SUCCESS)
            printf("[SpinnakerCamera] Unable to set node %s: %d\n", nodeName, err);

        if (IsAvailableAndReadable(attrNode, nodeName))
        {
            err = spinEnumerationGetEntryByName(attrNode, value, &valueNode);
            if (err != SPINNAKER_ERR_SUCCESS)
                printf("[SpinnakerCamera] Unable to set node %s to %s (enum entry retrieval): %d\n", nodeName, value, err);
        }

        int64_t actualValue = 0;
        if (IsAvailableAndReadable(valueNode, valueName)) spinEnumerationEntryGetIntValue(valueNode, &actualValue);
        if (IsAvailableAndWritable(attrNode, nodeName)) spinEnumerationSetIntValue(attrNode, actualValue);
    }

    void PrintRetrieveNodeFailure(std::string node, std::string name)
    {
        std::cout << "Unable to get " << node << " " << name << " " << node << "failed." << std::endl;
    }

    // This function helps to check if a node is available and readable
    bool8_t IsAvailableAndReadable(spinNodeHandle hNode, std::string nodeName)
    {
        bool8_t pbAvailable = False;
        spinError err = SPINNAKER_ERR_SUCCESS;
        err = spinNodeIsAvailable(hNode, &pbAvailable);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            std::cout <<  "Unable to retrieve node availability" << nodeName << std::endl;
        }

        bool8_t pbReadable = False;
        err = spinNodeIsReadable(hNode, &pbReadable);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            std::cout <<  "Unable to retrieve node availability" << nodeName << std::endl;
        }
        return pbReadable && pbAvailable;
    }

    // This function helps to check if a node is available and writable
    bool8_t IsAvailableAndWritable(spinNodeHandle hNode, std::string nodeName)
    {
        bool8_t pbAvailable = False;
        spinError err = SPINNAKER_ERR_SUCCESS;
        err = spinNodeIsAvailable(hNode, &pbAvailable);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            std::cout <<  "Unable to retrieve node availability" << nodeName << std::endl;
        }

        bool8_t pbWritable = False;
        err = spinNodeIsWritable(hNode, &pbWritable);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            std::cout <<  "Unable to retrieve node availability" << nodeName << std::endl;
        }
        return pbWritable && pbAvailable;
    }

public:
    spinCamera _camera;
    spinNodeMapHandle _hNodeMapTLDevice;
    spinNodeMapHandle _nodeMap, _streamNodeMap;
    spinNodeHandle _streamBufferMode;
    bool _acquisitionEnabled;

    int skip_frames = -1;
    int frame_counter = 0;
    
};

class SpinnakerManager
{
public:
    SpinnakerManager() : _numCameras(NULL)
    {
        spinError err = spinSystemGetInstance(&_system);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("[SpinnakerManager] Unable to retrieve system instance: %d\n", err);
            _system = NULL; return;
        }
        else
            spinSystemGetLibraryVersion(_system, &_libVersion);

        err = spinCameraListCreateEmpty(&_cameraList);
        if (err != SPINNAKER_ERR_SUCCESS)
            printf("[SpinnakerManager] Unable to create camera list: %d\n", err);
        else
        {
            err = spinSystemGetCameras(_system, _cameraList);
            if (err != SPINNAKER_ERR_SUCCESS)
                printf("[SpinnakerManager] Unable to retrieve camera list: %d\n", err);
            else
                spinCameraListGetSize(_cameraList, &_numCameras);
        }
    }

    ~SpinnakerManager()
    {
        if (_cameraList != NULL)
        {
            spinCameraListClear(_cameraList);
            spinCameraListDestroy(_cameraList);
        }
        if (_system != NULL)
            spinSystemReleaseInstance(_system);
    }

    size_t getNumCameras() const { return _numCameras; }
    void destroy(SpinnakerCamera* cam) { delete cam; }

    SpinnakerCamera* create(int index)
    {
        spinCamera camera = NULL;
        spinError err = spinCameraListGet(_cameraList, index, &camera);
        spinNodeMapHandle hNodeMapTLDevice = NULL;

        err = spinCameraGetTLDeviceNodeMap(camera, &hNodeMapTLDevice);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to retrieve TL device nodemap. Aborting with error %d...\n\n", err);
            return NULL;
        }


 		spinNodeHandle hDeviceVendorName = NULL;
        bool8_t deviceVendorNameIsAvailable = False;
        bool8_t deviceVendorNameIsReadable = False;

        // Retrieve node
        err = spinNodeMapGetNode(hNodeMapTLDevice, "DeviceVendorName", &hDeviceVendorName);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to retrieve device information (vendor name node). Aborting with error %d...\n\n", err);
            return NULL;
        }

        // Check availability
        err = spinNodeIsAvailable(hDeviceVendorName, &deviceVendorNameIsAvailable);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to check node availability (vendor name node). Aborting with error %d...\n\n", err);
            return NULL;
        }

        // Check readability
        err = spinNodeIsReadable(hDeviceVendorName, &deviceVendorNameIsReadable);
        if (err != SPINNAKER_ERR_SUCCESS)
        {
            printf("Unable to check node readability (vendor name node). Aborting with error %d...\n\n", err);
            return NULL;
        }

		char deviceVendorName[MAX_BUFF_LEN];
        size_t lenDeviceVendorName = MAX_BUFF_LEN;

        // Print device vendor name
        if (deviceVendorNameIsAvailable && deviceVendorNameIsReadable)
        {
            err = spinStringGetValue(hDeviceVendorName, deviceVendorName, &lenDeviceVendorName);
            if (err != SPINNAKER_ERR_SUCCESS)
            {
                printf("Unable to retrieve device information (vendor name value). Aborting with error %d...\n\n", err);
                return NULL;
            }
        }
        else
        {
            strcpy(deviceVendorName, "Not readable");
        }


        printf("\tDevice%d %s\n\n", index, deviceVendorName);

        
        if (std::string(deviceVendorName) != std::string("Point Grey Research"))
        {
            printf("[SpinnakerManager] Unable to select camera: %d\n", err);
            return NULL;
        }
        else
            return new SpinnakerCamera(camera);
    }

protected:
    spinSystem _system;
    spinLibraryVersion _libVersion;
    spinCameraList _cameraList;
    size_t _numCameras;
};

void *get_image_buffer(void *args)
{
	spinnaker_publisher *_this = (spinnaker_publisher*)args;

    SpinnakerManager spinnaker;
    size_t numCameras = spinnaker.getNumCameras();

    SpinnakerCamera* camera;
    for (size_t i = 0; i < numCameras; i++ ) {
        camera = spinnaker.create(i);
        if (NULL != camera)
            break; 
    }
    camera->setFrameRate(_this->frame_rate_);
    bool isRunning = true;
    while (isRunning)
    {
        ros::Time frame_time;
        cv::Mat frame = camera->getFrame(frame_time);
        if (frame.empty())
            continue;
        //cv::imshow("Camera" + std::to_string(i), frame);
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "rgb8", frame).toImageMsg();
        msg->header.stamp = frame_time;
        msg->header.frame_id = _this->camera_frameid_;
        _this -> image_publisher_.publish(msg);

        // sleep(0.05);
    }

    for (size_t i = 0; i < numCameras; ++i)
    {
        camera->endAcquisition();
        spinnaker.destroy(camera);
    }
}

void spinnaker_publisher::exposure_set_callback(const std_msgs::String& msg)
{
    std::string message = msg.data;
    std::vector<string> exposure_v;
    splitstring(message, exposure_v, "/");
    id_camera_set = atoi(exposure_v[0].c_str());
    exposure_mode = exposure_v[1];
    exposure_value = atoi(exposure_v[2].c_str());
    std::cout << "id_camera_set: " << id_camera_set << ", exposure_mode: " << exposure_mode << ", exposure_value: " << exposure_value << std::endl;
}

spinnaker_publisher::spinnaker_publisher()
{
    ros::NodeHandle private_node_handle("~");
    ros::NodeHandle node_handle_;

    private_node_handle.param<std::string>("camera_topic_name", camera_topic_name_, "/camera");
    private_node_handle.param<std::string>("camera_frameid", camera_frameid_, "spinnaker_picture");
   private_node_handle.param<float>("frame_rate", frame_rate_, 10.0f);

    private_node_handle.param<std::string>("heartbeat_topic_name", _heartbeat_topic_str, "/yida/heartbeat");


    heartbeat_pub_ = node_handle_.advertise<diagnostic_msgs::DiagnosticArray>(_heartbeat_topic_str, 1);

    image_transport::ImageTransport img_tranport(node_handle_);
    image_publisher_ = img_tranport.advertise(camera_topic_name_, 1);

    exposureset_sub_ = node_handle_.subscribe("/exposure_set", 1, &spinnaker_publisher::exposure_set_callback, this);

    create_work_thread();
}

spinnaker_publisher::~spinnaker_publisher()
{

}

void spinnaker_publisher::splitstring(const string& s, vector<string>& v, const string& c)
{
    string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while(string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2-pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }

    if(pos1 != s.length())
    {
        v.push_back(s.substr(pos1));
    }
}

void spinnaker_publisher::create_work_thread()
{
    pthread_t ros_thread = 0;
    pthread_create(&ros_thread,NULL,get_image_buffer,(void*)this);
}

void spinnaker_publisher::update()
{
        std::string message = "visible camera is ok!";
        std::string hardware_id = "visible_camera";
        pub_heartbeat(0, message, hardware_id);
}

void spinnaker_publisher::pub_heartbeat(int level, string message, string hardware_id)
{
    diagnostic_msgs::DiagnosticArray log;
    log.header.stamp = ros::Time::now();

    diagnostic_msgs::DiagnosticStatus s;
    s.name = __app_name__;         // 这里写节点的名字
    s.level = level;               // 0 = OK, 1 = Warn, 2 = Error
    if (!message.empty())
    {
        s.message = message;       // 问题描述
    }
    s.hardware_id = hardware_id;   // 硬件信息
    log.status.push_back(s);

    heartbeat_pub_.publish(log);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, __app_name__);
    spinnaker_publisher spinnaker_cap;
    ros::Rate rate(30);

    while (ros::ok())
    {
        spinnaker_cap.update();
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
