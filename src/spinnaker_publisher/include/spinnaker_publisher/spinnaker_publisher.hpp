#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#include "spinc/SpinnakerC.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <diagnostic_msgs/DiagnosticArray.h>

using namespace cv;
using namespace std;

#define __app_name__ "spinnaker_publisher_node"

class CStopWatch
 {
 public:
 	/// 构造函数 ms
 	CStopWatch()
 	{
 		//Start();
 	}

 	void Start()
 	{
 		gettimeofday(&m_tv, &m_tz);
        m_starttime = m_tv.tv_sec * 1000 + m_tv.tv_usec / 1000;
        //std::cout << "start time is " << m_starttime << std::endl;
 	}

 	double Stop(bool StartAgain)
 	{
 		gettimeofday(&m_tv, &m_tz);
        m_stoptime = m_tv.tv_sec * 1000 + m_tv.tv_usec / 1000; 
        //std::cout << "stop time is " << m_stoptime << std::endl;
 		double theElapsedTime = (double)ElapsedTime();
 		if(StartAgain)
 			m_starttime = m_stoptime; 
 		return theElapsedTime;
 	}

 	int ElapsedTime()
 	{
 		long differtime = 0;
        differtime = m_stoptime - m_starttime;
 		return differtime;
 	}
 	
 protected:
 	struct timeval m_tv;
    struct timezone m_tz;
    long m_starttime;
    long m_stoptime;  
};
 
class spinnaker_publisher
{
public:
    std::string _heartbeat_topic_str;

    std::string camera_topic_name_;
    std::string camera_frameid_;


    int id_camera_set, exposure_value;
    std::string exposure_mode;
    float frame_rate_;

    CStopWatch stopwatch;

    ros::Publisher heartbeat_pub_;

    ros::Subscriber exposureset_sub_; 

    image_transport::Publisher image_publisher_;

public:
    spinnaker_publisher();
    ~spinnaker_publisher();
    void create_work_thread();
    void update();
    void exposure_set_callback(const std_msgs::String& msg);
    void imudata_callback(const sensor_msgs::Imu& msg);
    void pub_heartbeat(int level, string message, string hardware_id);
    void splitstring(const string& s, vector<string>& v, const string& c);
};
