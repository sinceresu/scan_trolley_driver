
//  Copyright (c) 2003-2021 Xsens Technologies B.V. or subsidiaries worldwide.
//  All rights reserved.
//  
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//  
//  1.	Redistributions of source code must retain the above copyright notice,
//  	this list of conditions, and the following disclaimer.
//  
//  2.	Redistributions in binary form must reproduce the above copyright notice,
//  	this list of conditions, and the following disclaimer in the documentation
//  	and/or other materials provided with the distribution.
//  
//  3.	Neither the names of the copyright holders nor the names of their contributors
//  	may be used to endorse or promote products derived from this software without
//  	specific prior written permission.
//  
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
//  THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY OR
//  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.THE LAWS OF THE NETHERLANDS 
//  SHALL BE EXCLUSIVELY APPLICABLE AND ANY DISPUTES SHALL BE FINALLY SETTLED UNDER THE RULES 
//  OF ARBITRATION OF THE INTERNATIONAL CHAMBER OF COMMERCE IN THE HAGUE BY ONE OR MORE 
//  ARBITRATORS APPOINTED IN ACCORDANCE WITH SAID RULES.
//  

#ifndef IMUPUBLISHER_H
#define IMUPUBLISHER_H

#include "packetcallback.h"
#include <string>

#include <sensor_msgs/Imu.h>
#include <std_msgs/Time.h>

static std::string SYNC_TOPIC = std::string("/imu/sync");	//The port on which to send data

static void variance_from_stddev_param(std::string param, double *variance_out)
{
    std::vector<double> stddev;
    if (ros::param::get(param, stddev))
    {
        if (stddev.size() == 3)
        {
            auto squared = [](double x) { return x * x; };
            std::transform(stddev.begin(), stddev.end(), variance_out, squared);
        }
        else
        {
            ROS_WARN("Wrong size of param: %s, must be of size 3", param.c_str());
        }
    }
    else
    {
        memset(variance_out, 0, 3 * sizeof(double));
    }
}


static  const double SYNC_SECONDS = 5.;

struct ImuPublisher : public PacketCallback
{
    ros::Publisher pub;
    ros::Publisher sync_pub;

    std::string frame_id = DEFAULT_FRAME_ID;

    double orientation_variance[3];
    double linear_acceleration_variance[3];
    double angular_velocity_variance[3];

    ros::Time base_sys_time_;
    ros::Time base_imu_time_;
    bool synced;
    ros::Time last_imu_time;
    enum SyncState {
        UNSYNCED = 0,
        SYNCING,
        SYNCED
    };
    SyncState sync_state;

    ImuPublisher(ros::NodeHandle &node) :
        synced(false),
        sync_state(UNSYNCED)
    {
        int pub_queue_size = 5;
        ros::param::get("~publisher_queue_size", pub_queue_size);
        pub = node.advertise<sensor_msgs::Imu>("imu/data", pub_queue_size);
        ros::param::get("~frame_id", frame_id);

        // REP 145: Conventions for IMU Sensor Drivers (http://www.ros.org/reps/rep-0145.html)
        variance_from_stddev_param("~orientation_stddev", orientation_variance);
        variance_from_stddev_param("~angular_velocity_stddev", angular_velocity_variance);
        variance_from_stddev_param("~linear_acceleration_stddev", linear_acceleration_variance);

        sync_pub = node.advertise<std_msgs::Time>(SYNC_TOPIC, pub_queue_size);

    }

    void operator()(const XsDataPacket &packet, ros::Time timestamp)
    {
        static uint32_t SYNCOUT_MARKER = 0x1 << 22;
        bool quaternion_available = packet.containsOrientation();
        bool gyro_available = packet.containsCalibratedGyroscopeData();
        bool accel_available = packet.containsCalibratedAcceleration();

        if (packet.containsSampleTimeFine())
        {
            const uint32_t SAMPLE_TIME_FINE_HZ = 10000UL;
            const uint32_t ONE_GHZ = 1000000000UL;
            uint32_t sec, nsec, t_fine;

            t_fine = packet.sampleTimeFine();
            sec = t_fine / SAMPLE_TIME_FINE_HZ;
            nsec = (t_fine % SAMPLE_TIME_FINE_HZ) * (ONE_GHZ / SAMPLE_TIME_FINE_HZ);

            if (packet.containsSampleTimeCoarse())
            {
                sec = packet.sampleTimeCoarse();
            }
            ros::Time imu_sample_time(sec, nsec);

            timestamp = syncSampleTime(timestamp, imu_sample_time);
            // if (sync_state != SYNCED) 
            if (!synced) 
                return;

            // ROS_INFO("timestamp  : %lf, imu sample time : %lf, system sample time : %lf", timestamp.toSec(), imu_sample_time.toSec(), system_sample_time.toSec());
            if  (packet.containsStatus()) {
                uint32_t status = packet.status();
                if ((status & SYNCOUT_MARKER) != 0) {
                    ROS_INFO("syncout marker found!");
                    std_msgs::Time msg;
                    msg.data = timestamp;
                    sync_pub.publish(msg);
                }
            }

        }

        geometry_msgs::Quaternion quaternion;
        if (quaternion_available)
        {
            XsQuaternion q = packet.orientationQuaternion();

            quaternion.w = q.w();
            quaternion.x = q.x();
            quaternion.y = q.y();
            quaternion.z = q.z();
        }

        geometry_msgs::Vector3 gyro;
        if (gyro_available)
        {
            XsVector g = packet.calibratedGyroscopeData();
            gyro.x = g[0];
            gyro.y = g[1];
            gyro.z = g[2];
        }

        geometry_msgs::Vector3 accel;
        if (accel_available)
        {
            XsVector a = packet.calibratedAcceleration();
            accel.x = a[0];
            accel.y = a[1];
            accel.z = a[2];
        }

        // Imu message, publish if any of the fields is available
        if (quaternion_available || accel_available || gyro_available)
        {
            sensor_msgs::Imu msg;

            msg.header.stamp = timestamp;
            msg.header.frame_id = frame_id;

            msg.orientation = quaternion;
            if (quaternion_available)
            {
                msg.orientation_covariance[0] = orientation_variance[0];
                msg.orientation_covariance[4] = orientation_variance[1];
                msg.orientation_covariance[8] = orientation_variance[2];
            }
            else
            {
                msg.orientation_covariance[0] = -1; // mark as not available
            }

            msg.angular_velocity = gyro;
            if (gyro_available)
            {
                msg.angular_velocity_covariance[0] = angular_velocity_variance[0];
                msg.angular_velocity_covariance[4] = angular_velocity_variance[1];
                msg.angular_velocity_covariance[8] = angular_velocity_variance[2];
            }
            else
            {
                msg.angular_velocity_covariance[0] = -1; // mark as not available
            }

            msg.linear_acceleration = accel;
            if (accel_available)
            {
                msg.linear_acceleration_covariance[0] = linear_acceleration_variance[0];
                msg.linear_acceleration_covariance[4] = linear_acceleration_variance[1];
                msg.linear_acceleration_covariance[8] = linear_acceleration_variance[2];
            }
            else
            {
                msg.linear_acceleration_covariance[0] = -1; // mark as not available
            }

            pub.publish(msg);
        }
    }

    ros::Time syncSampleTime(const ros::Time& system_time, const ros::Time& imu_sample_time) {
        ros::Time ros_time = base_sys_time_ + (imu_sample_time - base_imu_time_);
        double resync_thresh_s = synced ? 10.0 : 0.1;
        double deskew = abs((ros_time - system_time).toSec());
        // double deskew = (ros_time - system_time).toSec();
        // switch (sync_state) {
        //     case UNSYNCED: {
        //         ROS_INFO("first sync, deskew: %lf, sys time: %lf,  imu time: %lf.", deskew, system_time.toSec(), imu_sample_time.toSec());
        //         ros_time = base_sys_time_ = system_time;
        //         base_imu_time_ = imu_sample_time;
        //         sync_state = SYNCING;
        //         break;
        //     }
        //     case SYNCING: {
        //         double resync_thresh_s = 0.1;
        //         if (abs(deskew) > resync_thresh_s){
        //             ROS_WARN("resync, deskew: %lf, sys time: %lf,  imu time: %lf.", deskew, system_time.toSec(), imu_sample_time.toSec());
        //             ros_time = base_sys_time_ = system_time;
        //             base_imu_time_ = imu_sample_time;
        //         } else {
        //             double passed_time = (system_time - base_sys_time_).toSec();
        //             if (passed_time > SYNC_SECONDS) {
        //                 sync_state = SYNCED;
        //                 ROS_INFO("transfer to SYNCED.");

        //             } else {
        //                 if (deskew > 0) {
        //                     ROS_INFO("adjust sync time back: %lf", deskew);
        //                     ros_time = base_sys_time_ = system_time;
        //                     base_imu_time_ = imu_sample_time;
        //                 }

        //             }
        //         }
        //         break;
        //     }
        //     default: {

        //         double resync_thresh_s = 10;

        //         if (abs(deskew) > resync_thresh_s){
        //             ROS_WARN("sync lost, deskew: %lf, sys time: %lf,  imu time: %lf.", deskew, system_time.toSec(), imu_sample_time.toSec());
        //             ros_time = base_sys_time_ = system_time;
        //             base_imu_time_ = imu_sample_time;
        //             sync_state = SYNCING;
        //             ROS_INFO("transfer to SYNCING.");

        //         } 
        //     }

        // }
        if (deskew > resync_thresh_s){
            ROS_WARN("diff betweent sys and imu time  : %lf, sys time: %lf,  imu time: %lf.", deskew, system_time.toSec(), imu_sample_time.toSec());
            ros_time = base_sys_time_ = system_time;
            base_imu_time_ = imu_sample_time;
            synced = false;
        } else {
            // ROS_INFO("diff betweent sys and imu time  : %lf.", deskew);
            synced = true;
        }
        return ros_time;
    }

};

#endif
