// Copyright 2016 Geoffrey Lawrence Viola

#ifndef NMEALIB_NMEABUILDER_HPP
#define NMEALIB_NMEABUILDER_HPP

#include <cstdint>
#include <string>
using namespace std;
string build_gprmc_utc(uint8_t const utc_hour, uint8_t const utc_minute,
                 double const utc_seconds);                      
string build_gpgaa_utc(uint8_t const utc_hour, uint8_t const utc_minute,
                 double const utc_seconds);                     
#endif // NMEALIB_NMEABUILDER_HPP
